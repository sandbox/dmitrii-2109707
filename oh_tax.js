(function($) {
  Drupal.behaviors.oh_tax = {
    attach: function(context, settings) {
      $('input.form-text.postal-code', context).change(function(event) {
        if ($(this).parents('.locality-block').find('select.state').val() == 'OH') {
          if (!$(this).val().match(/^(\d{5})[^\d]*(\d{4})$/)) {
            alert(Drupal.t('For accurate OH sale tax calculation please specify 9 digit ZIP Code.'));
          }
        }
      });
    }
  };
})(jQuery);
