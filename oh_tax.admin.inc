<?php

/**
 * @file
 * Administration page callbacks for the OH tax module.
 */

/**
 * The module settings form.
 *
 * @see oh_tax_admin_form_submit
 * @see oh_tax_admin_form_validate
 * @ingroup forms
 */
function oh_tax_admin_form($form, $form_state) {
  // Last update info.
  $date = variable_get('oh_tax_last_update', FALSE);
  if (!$date) {
    $date = t('Never');
  }
  else {
    $date = format_date($date, 'long');
  }
  $form['last_update'] = array(
    '#markup' => '<div>' . t('Last update: @date.', array('@date' => $date)) . '</div>',
  );
  // Last check for update info.
  $date = variable_get('oh_tax_last_check', FALSE);
  if (!$date) {
    $date = t('Never');
  }
  else {
    $date = format_date($date, 'long');
  }
  $form['last_check'] = array(
    '#markup' => '<div>' . t('Last check for tax update: @date.', array('@date' => $date)) . '</div>',
  );

  $form['check'] = array(
    '#title' => t('Check for updates'),
    '#type' => 'select',
    '#default_value' => variable_get('oh_tax_check_period'),
    '#options' => array(
      0 => t('Do not check for updates automatically'),
      60 * 60 * 24 => t('Daily'),
      60 * 60 * 24 * 7 => t('Weekly'),
      60 * 60 * 24 * 30 => t('Monthly'),
    ),
  );

  $form['url'] = array(
    '#title' => t('The csv-file url'),
    '#type' => 'text',
    '#default_value' => variable_get('oh_tax_url'),
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Import new data'),
    '#name' => 'update',
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#name' => 'save',
  );

  return $form;
}

/**
 * Form submission validator for oh_tax_admin_form().
 * @see oh_tax_admin_form
 * @ingroup forms
 */
function oh_tax_admin_form_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] != 'update' and !isset($form['check']['#options'][$form_state['values']['check']])) {
    form_set_error('check', t('Please select valid value from list.'));
  }
}

/**
 * Form submission handler for oh_tax_admin_form().
 * @see oh_tax_admin_form_validate
 * @see oh_tax_admin_form
 * @ingroup forms
 */
function oh_tax_admin_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'update') {
    if(_oh_tax_update(TRUE)) {
      drupal_set_message(t('The OH tax data downloaded successfully.'));
    }
  }
  else {
    variable_set('oh_tax_check_period', $form_state['values']['check']);
  }
}
