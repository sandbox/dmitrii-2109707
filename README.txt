The module provide schema to import and store OH sales tax data.
Data source: https://thefinder.tax.ohio.gov/StreamlineSalesTaxWeb/Default.aspx

Direct url for import tax data:
https://thefinder.tax.ohio.gov/StreamlineSalesTaxWeb/Download/BoundaryData/CountySalesTaxRateReportPlus4.csv

File format:
Zip,Zip4Low,Zip4High,County,Rate
43001,0000,9999,Licking,0.07
43002,0000,9999,Franklin,0.0675
43003,0000,0660,Delaware,0.0675
43003,7000,7001,Morrow,0.07
43003,8000,8740,Delaware,0.0675
43003,9000,9000,Morrow,0.07
...

Also you must setup your commerce rules to use OH sales tax.
