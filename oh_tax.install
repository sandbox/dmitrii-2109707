<?php

/**
 * @file
 * Install, update and uninstall functions for the OH tax module.
 */

/**
 * Implements hook_uninstall().
 */
function oh_tax_uninstall() {
  variable_del('oh_tax_last_update');
  variable_del('oh_tax_last_check');
  variable_del('oh_tax_check_period');
  variable_del('oh_tax_url');
}

/**
 * Implements hook_install().
 */
function oh_tax_install() {
  variable_set('oh_tax_check_period', 60 * 60 * 24);  // Daily
  variable_set('oh_tax_url', 'https://thefinder.tax.ohio.gov/StreamlineSalesTaxWeb/Download/BoundaryData/CountySalesTaxRateReportPlus4.csv');
  // Fill OH tax database.
  drupal_set_message(st('The OH tax module installed successfully.') . ' ' . l(st('Check settings.'), 'admin/config/services/oh_tax'));
  if (_oh_tax_update(TRUE)) {
    drupal_set_message(st('The OH tax data downloaded successfully.'));
  }
}

/**
 * Implements hook_schema().
 */
function oh_tax_schema() {
  $schema['oh_tax'] = array(
    'description' => 'OH tax database.',
    'fields' => array(
      'zip' => array(
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'medium',
        'description' => 'Zip Code.',
      ),
      'zip4low' => array(
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'medium',
        'description' => 'Zip4Low.',
      ),
      'zip4high' => array(
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'medium',
        'description' => 'Zip4High.',
      ),
      'county' => array(
        'type' => 'varchar',
        'length' => 80,
        'not null' => TRUE,
        'default' => '',
        'description' => 'County.',
      ),
      'rate' => array(
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => TRUE,
        'default' => 0,
        'precision' => 6,
        'scale' => 4,
        'description' => 'Rate.',
      ),
    ),
    'primary key' => array('zip', 'zip4low', 'zip4high'),
    'indexes' => array(
      'county' => array('county'),
    ),
  );

  return $schema;
}
